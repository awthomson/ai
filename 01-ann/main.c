#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

// Defines
#define NUM_INPUT_NODES 2		// number of neurons in the input layer
#define NUM_HIDDEN_NODES 4			// number of neurons in the hidden layer
#define NUM_OUTPUT_NODES 2			// number of neurons in the output layer
#define NUM_TRAINING_POINTS 4		// number of training samples
#define MAX_EPOCHS 50000
#define COST_LIMIT 0.001f

// Function definitions
double sigmoid(double);

// Structure of training data
struct{
	double input[NUM_INPUT_NODES];
	double teach[NUM_OUTPUT_NODES];
} data[NUM_TRAINING_POINTS];

int main(){

	double input[NUM_INPUT_NODES];									// input layer
	double hidden[NUM_HIDDEN_NODES];								// hidden layer
	double output[NUM_OUTPUT_NODES];         						// output layer
	double expected_out[NUM_OUTPUT_NODES];							// expected output layer
	double weights_hidden[NUM_INPUT_NODES][NUM_HIDDEN_NODES];		// weights from input layer to hidden layer
	double weights_out[NUM_HIDDEN_NODES][NUM_OUTPUT_NODES];	
	double delta_weights_hidden[NUM_INPUT_NODES][NUM_HIDDEN_NODES];  
	double delta_weights_out[NUM_HIDDEN_NODES][NUM_OUTPUT_NODES];	
	double hidden_delta[NUM_HIDDEN_NODES];
	double output_delta[NUM_OUTPUT_NODES];
	double alpha = 0.01f;
	double beta = 0.01f;
	
	srand(time(NULL));
	
	// Generate data samples
	data[0].input[0] = 0.0f;
	data[0].input[1] = 1.0f;
	data[0].teach[0] = 1.0f;
	data[0].teach[1] = 0.0f;

	data[1].input[0] = 0.0f;
	data[1].input[1] = 1.0f;
	data[1].teach[0] = 1.0f;
	data[1].teach[1] = 0.0f;

	data[2].input[0] = 0.0f;
	data[2].input[1] = 1.0f;
	data[2].teach[0] = 1.0f;
	data[2].teach[1] = 0.0f;

	data[3].input[0] = 1.0f;
	data[3].input[1] = 1.0f;
	data[3].teach[0] = 0.0f;
	data[3].teach[1] = 0.0f;


	// Initialization
	for (int i=0; i<NUM_INPUT_NODES; i++){
		for (int j=0; j<NUM_HIDDEN_NODES; j++){
			weights_hidden[i][j] = ((float)rand()/(float)RAND_MAX);
			delta_weights_hidden[i][j] = 0;
		}
	}
	for (int h=0; h<NUM_HIDDEN_NODES; h++){
		for (int o=0; o<NUM_OUTPUT_NODES; o++){
			weights_out[h][o] = ((float)rand()/(float)RAND_MAX);
			delta_weights_out[h][o] = 0;
		}
	}

	// Training
	int epoch = 0;
	while (epoch++ < MAX_EPOCHS) {
		
		double total_cost = 0.0f;

		for(int t=0; t<NUM_TRAINING_POINTS; t++){

			// Feed the input nodes with training data
			for(int i=0; i<NUM_INPUT_NODES; i++){
				input[i] = data[t].input[i];
			}

			/***********************************************************
			 * Feed forward
			 **********************************************************/

			for (int o=0; o<NUM_OUTPUT_NODES ; o++) {
				expected_out[o] = data[t].teach[o];
			}

			for (int h=0; h<NUM_HIDDEN_NODES; h++) {
				double temp_sum = 0.0f;
				for(int i=0; i<NUM_INPUT_NODES; i++)
					temp_sum += weights_hidden[i][h] * input[i];
				hidden[h] = sigmoid(temp_sum);
			}

			for (int o=0; o<NUM_OUTPUT_NODES; o++) {
				double temp_sum = 0.0f;
				for (int h=0; h<NUM_HIDDEN_NODES; h++)
					temp_sum += weights_out[h][o] * hidden[h];
				output[o] = sigmoid(temp_sum);
			}

			/***********************************************************
			 * Back Propagation
			 **********************************************************/
			for(int o=0; o<NUM_OUTPUT_NODES; o++){
				double temp_error = expected_out[o] - output[o];
				output_delta[o] = -temp_error * sigmoid(output[o]) * (1.0 - sigmoid(output[o]));
				total_cost += temp_error * temp_error;
			}
			total_cost = total_cost / 2;

			for (int h=0; h<NUM_HIDDEN_NODES; h++){
				double temp_error = 0.0f;
				for (int o=0; o<NUM_OUTPUT_NODES; o++)
					temp_error += output_delta[o] * weights_out[h][o];
				hidden_delta[h] = temp_error * (1.0 + hidden[h]) * (1.0 - hidden[h]);
			}

			// Calculate how much to adjust weights in output layer and adjust them
			for (int o=0; o<NUM_OUTPUT_NODES; o++) {
				for (int h=0; h<NUM_HIDDEN_NODES; h++) {
					delta_weights_out[h][o] = alpha * delta_weights_out[h][o] + beta * output_delta[o] * hidden[h];
					weights_out[h][o] -= delta_weights_out[h][o];
				}
			}

			// Calculate how much to adjust weights in hidden layer and adjust them
			for (int h=0; h<NUM_HIDDEN_NODES; h++){
				for (int i=0; i<NUM_INPUT_NODES; i++){
					delta_weights_hidden[i][h] = alpha * delta_weights_hidden[i][h] + beta * hidden_delta[h] * input[i];
					weights_hidden[i][h] -= delta_weights_hidden[i][h];
				}
			}
			
		} // t
		
		if (epoch%5000==0) {
			printf("%.1f%% complete.  Cost so far is %f\n", (float)epoch/(float)MAX_EPOCHS*100.0f, total_cost);
		}
		
		// Finish if we're lower than the set threshold
		if (total_cost <= COST_LIMIT) {
			printf("Training complete after %d epochs.\n", epoch);
			printf("Current cost is %f\n\n", total_cost);
			break;
		}
			
	}

}

// Calulcate sigmoid
double sigmoid(double x){
	return(1.0 / (1.0 + exp(-x)));
}
