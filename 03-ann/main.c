#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>		// strcmp
#include "ann.h"

void show_usage(void);


int main(int argc, char *argv[]	) {
	
	if (argc < 2) {
		show_usage();
		return 0;
	}

	struct NN_Model my_model;
	struct NN_Training_Data my_training_data;
	
	if (strcmp(argv[1], "train")==0) {
		
		// Train a neural network
		if (argc < 4) {
			show_usage();
			return 0;
		}
		// Train model from file and save/display it
		srand(time(NULL));
		read_training_data_from_file(&my_model, &my_training_data, argv[2]);
		model_train(&my_model, &my_training_data);
		model_save(&my_model, argv[3]);

		training_delete(&my_training_data);
		model_delete(&my_model);
		
	} else if (strcmp(argv[1], "query")==0) {
		
		// Query a pre-trained network
		read_model_from_file(&my_model, argv[2]);
		int num_params = argc - 3;
		float *inputs = malloc(num_params * sizeof(double));
		if (num_params != my_model.num_input_nodes) {
			printf("[FATAL] Expecting %d parameter(s) but get %d.\n", my_model.num_input_nodes, num_params);
			return 1;
		}
		for (int i=0; i < my_model.num_input_nodes; i++) {
			inputs[i] = atof(argv[3+i]);
		}
		model_query(&my_model, inputs);
		model_delete(&my_model);
		
	} else {
		show_usage();
		return 0;
	}
	
	
}
	
void show_usage(void) {
	printf("USAGE:\n");
	printf("  nn train infile [outfile]\n");
	printf("  nn query infile INPUTS\n"); 
}
