# Basic Neural Network Demo

This is a basic NN example with back propagation and one hidden layer to train a neural network to return the inverse of the input.

The network is structured with:
 * 2 input nodes
 * 4 nodes in the hidden layer
 * 2 output nodes

The training data contains the 4 possible input combinations and the expected output:
 * 00 --> 11
 * 01 --> 10
 * 10 --> 01
 * 11 --> 00

This is written to be as simple as possible to follow, possibly at the expense of having highly-optimised, reusable code!

Compile with:
~~# gcc main.c -lm~~

Execute with:
~~# ./a.out~~
