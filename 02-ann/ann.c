#include <math.h>		// exp
#include <stdio.h>		// printf
#include <stdlib.h>		// malloc

#include "ann.h"

void model_train(struct NN_Model *model, struct NN_Training_Data *data, int max_epochs, float alpha, float beta, float limit) {
	
	int epoch = 0;
	double total_cost = 0.0f;
	
	while (epoch++ < max_epochs) {
	
		for(int t=0; t<data->num_training_points; t++){

			// Feed the input nodes with training data
			for (int i=0; i<model->num_input_nodes; i++) {
				model->input_node[i].value = data->training_point[t].input[i];
			}
			
			// Feed forward.  Input nodes to Hidden layer
			for (int h=0; h < model->num_hidden_nodes; h++) {
				double temp_sum = 0.0f;
				for(int i=0; i < model->num_input_nodes; i++)
					// temp_sum += weights_hidden[i][h] * input[i];
					temp_sum += model->hidden_node[h].weight[i] * model->input_node[i].value;
				model->hidden_node[h].value = sigmoid(temp_sum);
			}
			
			// Feed forward.  Hidden nodes to Output layer
			for (int o=0; o < model->num_output_nodes; o++) {
				double temp_sum = 0.0f;
				for (int h=0; h < model->num_hidden_nodes; h++)
					temp_sum += model->output_node[o].weight[h] * model->hidden_node[h].value;
				model->output_node[o].value = sigmoid(temp_sum);
			}
			
			// Back Propagation - determine total cost, and how much the output nodes are off
			for (int o=0; o < model->num_output_nodes; o++) {
				double temp_error = data->training_point[t].expected[o] - model->output_node[o].value;
				model->output_node[o].delta = -temp_error * sigmoid(model->output_node[o].value) * (1.0 - sigmoid(model->output_node[o].value));
				total_cost += temp_error * temp_error;
			}
			total_cost = total_cost / 2;
			
			// Back Propagation - how much are the hidden layer nodes off
			for (int h=0; h < model->num_hidden_nodes; h++) {
				double temp_error = 0.0f;
				for (int o=0; o < model->num_output_nodes; o++) {
					temp_error += model->output_node[o].delta * model->output_node[o].weight[h];
				}
				model->hidden_node[h].delta = temp_error * (1.0 + model->hidden_node[h].value) * (1.0 - model->hidden_node[h].value);
			}
			
			// Back Propagation - Apply changes to weights in output layer
			for (int o=0; o < model->num_output_nodes; o++) {
				for (int h=0; h < model->num_hidden_nodes; h++) {
					model->output_node[o].weight_delta[h] = alpha * model->output_node[o].weight_delta[h] + beta * model->output_node[o].delta * model->hidden_node[h].value;
					model->output_node[o].weight[h] -= model->output_node[o].weight_delta[h];
				}
			}
			
			// Back Propagation - Apply changes to weights in hidden layer
			for (int h=0; h < model->num_hidden_nodes; h++) {
				for (int i=0; i<model->num_input_nodes; i++) {
					model->hidden_node[h].weight_delta[i] = alpha * model->hidden_node[h].weight_delta[i] + beta * model->hidden_node[h].delta * model->input_node[i].value;
					model->hidden_node[h].weight[i] -= model->hidden_node[h].weight_delta[i];
				}
			}
			
		} // t
			
		if (total_cost <= limit) {
			break;
		}
			
	} // epoch

	if (total_cost <= limit) {
		printf("Successfully trained after %d epochs\n", epoch);
	} else {
		printf("Terminated after reaching epoch limit.  Final cost is %f.\n", total_cost);
	}
	
}

void model_init(struct NN_Model *model, int num_input, int num_hidden, int num_output) {
	model->num_input_nodes = num_input;
	model->num_hidden_nodes = num_hidden;
	model->num_output_nodes = num_output;
	
	model->input_node = malloc(num_input * sizeof(struct NN_Input_Node));
	model->hidden_node = malloc(num_hidden * sizeof(struct NN_Node));
	model->output_node = malloc(num_output * sizeof(struct NN_Node));
	
	// Init hidden layer
	for (int h=0; h<num_hidden; h++) {
		model->hidden_node[h].weight = malloc(num_input * sizeof(double));
		model->hidden_node[h].weight_delta = malloc(num_input * sizeof(double));
	}
	for (int h=0; h<num_hidden; h++) {
		for (int i=0; i<num_input; i++) {
			model->hidden_node[h].weight[i] = ((float)rand()/(float)RAND_MAX);
			model->hidden_node[h].weight_delta[i] = 0.0f;
		}
	}
	
	// Init Ouput layer
	for (int o=0; o < num_output; o++) {
		model->output_node[o].weight = malloc(num_hidden * sizeof(double));
		model->output_node[o].weight_delta = malloc(num_hidden * sizeof(double));
	}
	for (int o=0; o<num_output; o++) {
		for (int h=0; h<num_hidden; h++) {
			model->output_node[o].weight[h] = ((float)rand()/(float)RAND_MAX);
			model->output_node[o].weight_delta[h] = 0.0f;
		}
	}
	
	return;
}

void model_info(struct NN_Model *model) {
	printf("[DEBUG} - Model > Metadata\n");
	printf("    Input Nodes: %d\n", model->num_input_nodes);
	printf("   Hidden Nodes: %d\n", model->num_hidden_nodes);
	printf("   Output Nodes: %d\n", model->num_output_nodes);
	printf("[DEBUG] - Model > Input Nodes\n");
	for (int i=0; i<model->num_input_nodes; i++)
		printf("   %d: %f\n", i, model->input_node[i].value);
	printf("[DEBUG] - Model > Hidden Nodes\n");
	for (int h=0; h<model->num_hidden_nodes; h++) {
		printf("   %d: V = %f   W = [ ", h, model->hidden_node[h].value);
		for (int i=0; i < model->num_input_nodes; i++)
			printf("%f ", model->hidden_node[h].weight[i]);
		printf("]   dW = [ ");
		for (int i=0; i < model->num_input_nodes; i++)
			printf("%f ", model->hidden_node[h].weight_delta[i]);
		printf("]\n");
	}
	printf("[DEBUG] - Model > Output Nodes\n");
	for (int o=0; o < model->num_output_nodes; o++) {
		printf("   %d: V = %f   W = [ ", o, model->output_node[o].value);
		for (int h=0; h < model->num_hidden_nodes; h++)
			printf("%d, %f", h, model->output_node[o].weight[h]);
		printf("]   dW = [ ");
		for (int h=0; h < model->num_hidden_nodes; h++)
			printf("%f ", model->output_node[o].weight_delta[h]);
		printf("]\n");
	}
	
}

void model_delete(struct NN_Model *model) {
	for (int o=0; o < model->num_output_nodes; o++) {
		free(model->output_node[o].weight);
		free(model->output_node[o].weight_delta);
	}
	for (int h=0; h < model->num_hidden_nodes; h++) {
		free(model->hidden_node[h].weight);
		free(model->hidden_node[h].weight_delta);
	}
	free(model->input_node);
	free(model->hidden_node);
	free(model->output_node);
}

void training_delete(struct NN_Training_Data *data) {
	for (int t=0; t < data->num_training_points; t++) {
		free(data->training_point[t].input);
		free(data->training_point[t].expected);
	}
	free(data->training_point);
}

// TODO - Read this in from a file
void training_data_init(struct NN_Training_Data *data) {
	data->num_training_points = 4;
	data->training_point = malloc(data->num_training_points * sizeof(struct NN_Training_Data));
	for (int t=0; t<data->num_training_points; t++) {
		data->training_point[t].input = malloc(2 * sizeof(float));
		data->training_point[t].expected = malloc(2 * sizeof(float));
	}
	
	// Hard coded training data...
	data->training_point[0].input[0] = 0.0f;
	data->training_point[0].input[1] = 0.0f;
	data->training_point[0].expected[0] = 1.0f;
	data->training_point[0].expected[1] = 1.0f;

	data->training_point[3].input[0] = 0.0f;
	data->training_point[3].input[1] = 1.0f;
	data->training_point[3].expected[0] = 1.0f;
	data->training_point[3].expected[1] = 0.0f;

	data->training_point[2].input[0] = 1.0f;
	data->training_point[2].input[1] = 0.0f;
	data->training_point[2].expected[0] = 0.0f;
	data->training_point[2].expected[1] = 1.0f;

	data->training_point[1].input[0] = 1.0f;
	data->training_point[1].input[1] = 1.0f;
	data->training_point[1].expected[0] = 0.0f;
	data->training_point[1].expected[1] = 0.0f;

}

void training_data_info(struct NN_Training_Data *data) {
	printf("*** [DEBUG} - Training Data\n");
	for (int t=0; t<data->num_training_points; t++) {
		struct NN_Training_Point *tp = &data->training_point[t];
		// TODO: This is hard coded to two input and 1 output training node
		printf("   %d: %f %f -> %f\n", t, tp->input[0], tp->input[1], tp->expected[0]);
	}
}

// Calulcate sigmoid
double sigmoid(double x){
	return(1.0 / (1.0 + exp(-x)));
}
