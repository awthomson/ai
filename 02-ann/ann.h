#ifndef __ann_h__
#define __ann_h__

struct NN_Training_Point {
	double *input;
	double *expected;
};

struct NN_Training_Data {
	int num_training_points;
	struct NN_Training_Point *training_point;
};

struct NN_Input_Node {
	double value;
};

struct NN_Node {
	double bias;
	double *weight;
	double *weight_delta;
	double value;
	double delta;
};

struct NN_Model {
	int num_input_nodes;
	int num_hidden_nodes;
	int num_output_nodes;
	struct NN_Input_Node *input_node;
	struct NN_Node *hidden_node;
	struct NN_Node *output_node;
};

void model_init(struct NN_Model *model, int num_input, int num_hidden, int num_output);
void model_info(struct NN_Model *model);
void model_delete(struct NN_Model *model);
void training_data_init(struct NN_Training_Data *data);
void training_data_info(struct NN_Training_Data *data);
void model_train(struct NN_Model *model, struct NN_Training_Data *data, int max_epochs, float alpha, float beta, float limit);
double sigmoid(double);

#endif
