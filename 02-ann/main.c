#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "ann.h"




int main(void) {
	
	// Setup

	srand(time(NULL));
	
	struct NN_Model my_model;
	struct NN_Training_Data my_training_data;
	model_init(&my_model, 2, 4, 2);
	training_data_init(&my_training_data);
	
	// Execute
	model_train(&my_model, &my_training_data, 20000, 0.05f, 0.8f, 0.01f);

	// Clean up
	model_delete(&my_model);
	
}
	
